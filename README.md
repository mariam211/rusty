# Description
Different algorithms to implement "sum zero" using:
- Rust
- Rayon library

# Instuctions:

- To set the number of element pass it as an argument `cargo run <number>`

- The svg files for all parallel algorithms can be found in the data folder.
