use itertools::Itertools;
use diam::prelude::*;
use rayon::prelude::*;
//use math::round;
use diam::join;
use num_traits::pow;
use std::env;

fn sum_zero(n: usize) -> Vec<i32> {
    let nb_elements = n as i32;
    let start = -(nb_elements/2);
    let end = (nb_elements/2) + 1;

    (start..end)
    .into_iter()
    .filter(|e| (*e!=0 && n%2 == 0) || n%2 == 1)
    .collect()
}

fn par_sum_zero(n: usize) -> Vec<i32> {
    let nb_elements = n as i32;
    let start = -(nb_elements/2);
    let end = (nb_elements/2) + 1;

    (start..end)
    .into_par_iter()
    .filter(|e| (*e!=0 && n%2 == 0) || n%2 == 1)
    .log("sum zero")
    .collect()
}

fn par_sum_zero_2(n: usize) -> Vec<i32> {
    let nb_elements = n as i32;
    let start = -(nb_elements/2);
    let end = (nb_elements/2) + 1;

    if n%2 == 1{
        (start..end)
        .into_par_iter()
        .log("sum zero")
        .collect()
    }else{
        (start..end)
        .into_par_iter()
        .filter(|e| (*e!=0))
        .log("sum zero")
        .collect()
    }
    
}

fn par_sum_zero_3(n: usize) -> Vec<i32> {
    let nb_elements = n as i32;
    let start = -(nb_elements/2);
    let end = (nb_elements/2) + 1;

    let mut left_array: Vec<i32>; 
    let right_array: Vec<i32>;
    
    (left_array, right_array) = join(
        || (start..nb_elements%2).into_par_iter().log("sum zero 3").collect::<Vec<i32>>(),
        || (1..end).into_par_iter().log("sum zero 3").collect::<Vec<i32>>(),
    );

    left_array.extend(&right_array);

    return left_array;
}

fn recurs_sum_zero(arr_size: usize) -> Vec<i32>{
    let mut nb_elements = arr_size as i32;

    if nb_elements == 1{
        return vec![0];
    }
    
    //if odd take the lower biggest even number
    nb_elements = nb_elements - (nb_elements%2);

    let levels:i32 = (nb_elements as f32).log2().ceil() as i32;

    let nb_leaves:i32 = pow(2i32, levels as usize);
    let unneeded_leaves:i32 = nb_leaves - nb_elements; //we will add zero at end if odd
    let max_number = nb_leaves*2 - 1 - unneeded_leaves*2; 
    
    
    let mut v:Vec<i32> = inner_recurs_sum_zero(nb_leaves, levels, nb_leaves, nb_leaves/2, max_number);

    //if odd number of elements we add 0 to the collection
    if arr_size%2 == 1{
        v.extend(vec![0]);
    }

    return v ;
}

fn inner_recurs_sum_zero(start_number: i32, levels: i32, number: i32, period: i32, max_number: i32) -> Vec<i32>{
    
    //minimun resulting number from this branching should not be bigger than our max wanted number
    if start_number - (pow(2i32, levels as usize) - 1) >  max_number{
        return Vec::new();
    }
    else if levels == 0{
        return vec![number];
    }
    else {
        let mut left_array: Vec<i32>; 
        let right_array: Vec<i32>;

        (left_array, right_array) = join(
            || inner_recurs_sum_zero(start_number.abs() - period, levels - 1, -start_number, period/2, max_number),
            || inner_recurs_sum_zero(start_number.abs() + period, levels - 1, start_number, period/2, max_number),
        );

        left_array.extend(&right_array);
        
        return left_array;
    }
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let arg = &args[1];
    let nb_integers: usize = arg.parse::<usize>().unwrap();

    let length:usize = nb_integers as usize;
    let start = std::time::Instant::now();
    let v = sum_zero(length);
    let seq_time = start.elapsed();

    eprintln!(
        "generating {} unique integers in sequential took {:?} sum",
        v.len(),
        //v,
        seq_time,
    );

    let vv: Vec<i32> = v.clone().into_iter().unique().collect::<Vec<i32>>();
    assert_eq!(v.len(), length);
    assert_eq!(v,  vv);
    assert_eq!(v.clone().into_iter().sum::<i32>(),  0);

    //Parallel
    let par_start = std::time::Instant::now();
    let par_v = par_sum_zero(length);
    let par_time = par_start.elapsed();
    let speedup_par = seq_time.as_secs_f32()/par_time.as_secs_f32();
    
    eprintln!(
        "generating {} unique integers in parallel took {:?}  speedup {:?} sum ",
        par_v.len(),
        //par_v,
        par_time,
        speedup_par,
    ); 

    assert_eq!(par_v.len(), length);
    assert_eq!(par_v,  vv);
    assert_eq!(par_v.clone().into_iter().sum::<i32>(),  0);

    diam::display_svg(|| {
        par_sum_zero(length);
        })
    .expect("failed saving svg file");

    //Parallel 2
    let par2_start = std::time::Instant::now();
    let par_v2 = par_sum_zero_2(length);
    let par2_time = par2_start.elapsed();
    let speedup_par2 = seq_time.as_secs_f32()/par2_time.as_secs_f32();

    eprintln!(
        "generating {} unique integers in parallel 2 took {:?} speedup {:?} sum ",
        par_v2.len(),
        //par_v2,
        par2_time,
        speedup_par2,
    ); 

    assert_eq!(par_v2.len(), length);
    assert_eq!(par_v2,  vv);
    assert_eq!(par_v2.clone().into_iter().sum::<i32>(),  0);

    diam::display_svg(|| {
        par_sum_zero_2(length);
        })
    .expect("failed saving svg file");

    //Parallel 3
    let par3_start = std::time::Instant::now();
    let par_v3 = par_sum_zero_3(length);
    let par3_time = par3_start.elapsed();
    let speedup_par3 = seq_time.as_secs_f32()/par3_time.as_secs_f32();

    eprintln!(
        "generating {} unique integers in parallel 3 took {:?} speedup {:?} sum ",
        par_v3.len(),
        //par_v3,
        par3_time,
        speedup_par3,
    ); 

    assert_eq!(par_v3.len(), length);
    assert_eq!(par_v3,  vv);
    assert_eq!(par_v3.clone().into_iter().sum::<i32>(),  0);

    diam::display_svg(|| {
        par_sum_zero_3(length);
        })
    .expect("failed saving svg file");
    
    //Recursive
    let nb:f32 = nb_integers as f32;
    let levels:i32 = nb.log2().ceil() as i32;
    let mut nb_elements = nb as i32;
    nb_elements = nb_elements - (nb_elements%2);
    let nb_leaves:i32 = pow(2i32, levels as usize);
    let unneeded_leaves:i32 = nb_leaves - nb_elements;
    let max_number = nb_leaves*2 - 1 - unneeded_leaves*2;
    
    eprintln!(
        "levels {} leaves {} unneeded leaves {} max {}",
       levels,
       nb_leaves,
       unneeded_leaves,
       max_number
    );

    let recursive_start = std::time::Instant::now();
    let buffer: Vec<i32> = recurs_sum_zero(length);
    let recursive_time = recursive_start.elapsed();
    let speedup_recurs = seq_time.as_secs_f32()/recursive_time.as_secs_f32();

    eprintln!(
            "generating {} unique integers took {:?} speedup {} sum ",
            buffer.len(),
            //buffer,
            recursive_time,
            speedup_recurs,
        ); 
    
    let bb: Vec<i32> = buffer.clone().into_iter().unique().collect::<Vec<i32>>();

    assert_eq!(buffer.len(), length);
    assert_eq!(buffer,  bb);
    assert_eq!(buffer.clone().into_iter().sum::<i32>(),  0);

    diam::display_svg(|| {
        recurs_sum_zero(length);
        })
    .expect("failed saving svg file");
}
